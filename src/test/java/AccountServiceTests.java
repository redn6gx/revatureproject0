import models.Account;
import models.CheckingAccount;
import models.User;
import org.junit.jupiter.api.*;
import repositories.AccountRepo;
import repositories.AccountRepoDAO;
import repositories.UserRepo;
import repositories.UserRepoDAO;
import services.AccountService;
import services.AccountServiceImpl;
import services.UserService;
import services.UserServiceImpl;
import util.GenericLinkedList;
import util.ResourceNotFoundException;

import static org.junit.jupiter.api.Assertions.*;

public class AccountServiceTests {
    private static AccountRepo ar;
    private static AccountService as;
    private static UserRepo ur;
    private static UserService us;
    private static User testUser;
    private static User testUserGetAllAccounts;

    @BeforeAll
    public static void setUp() {
        ar = new AccountRepoDAO();
        as = new AccountServiceImpl(ar);
        ur = new UserRepoDAO();
        us = new UserServiceImpl(ur);

        testUser = us.addUser(new User("UnitTest", "UnitTest"));
        testUserGetAllAccounts = us.addUser(new User("UnitTestAll", "UnitTestAll"));
        System.out.println(testUser.getUserID() + "," + testUser.getUsername() + ", " + testUser.getPassword());
    }

    @Test
    public void addAccountSuccessTest() {
        Account acc = as.addAccount(new CheckingAccount("TestAccount1", testUser.getUserID()));

        assertEquals("TestAccount1", acc.getAccountName());
        assertEquals(testUser.getUserID(), acc.getUserID());
        assertNotNull(acc.getAccountID());
        assertEquals(0, acc.getBalance());
    }
    @Test
    public void getAccountTest() {
        Account acc = as.addAccount(new CheckingAccount("TestAccount2", testUser.getUserID()));

        Account tempAcc = as.getAccount(acc.getAccountID());
        assertEquals(acc.getAccountID(), as.getAccount(acc.getAccountID()).getAccountID());
    }
    @Test
    public void updateBalanceTest() {
        Account acc = as.addAccount(new CheckingAccount("TestAccount3", testUser.getUserID()));

        double balance = as.updateBalance(1000.50, acc.getAccountID());
        assertEquals(1000.50, balance);
    }
    @Test
    public void deleteAccountTest() throws ResourceNotFoundException{
        Account acc = as.addAccount(new CheckingAccount("TestAccount4", testUser.getUserID()));

        as.deleteAccount(acc.getAccountID());
        assertNull(as.getAccount(acc.getAccountID()));
    }
    @Test
    public void getAllAccountsTest() {
        as.addAccount(new CheckingAccount("TestAccount5", testUserGetAllAccounts.getUserID()));
        as.addAccount(new CheckingAccount("TestAccount6", testUserGetAllAccounts.getUserID()));
        as.addAccount(new CheckingAccount("TestAccount7", testUserGetAllAccounts.getUserID()));
        as.addAccount(new CheckingAccount("TestAccount8", testUserGetAllAccounts.getUserID()));

        GenericLinkedList<Account> testAccounts = as.getAllAccounts(testUserGetAllAccounts.getUserID());
        assertEquals(4, testAccounts.getSize());
    }

    @AfterAll
    public static void cleanUp() throws ResourceNotFoundException {
        //deletes testUser and all associated accounts created through cascading
        us.deleteUser(testUser.getUserID());
        us.deleteUser(testUserGetAllAccounts.getUserID());
    }
}
