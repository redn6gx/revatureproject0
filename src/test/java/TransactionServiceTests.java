import models.Account;
import models.CheckingAccount;
import models.Transaction;
import models.User;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import repositories.*;
import services.*;
import util.GenericLinkedList;
import util.ResourceNotFoundException;

import static org.junit.jupiter.api.Assertions.*;

public class TransactionServiceTests {
    private static TransactionRepo tr;
    private static TransactionService ts;
    private static AccountRepo ar;
    private static AccountService as;
    private static UserRepo ur;
    private static UserService us;
    private static User testUser;
    private static Account testAccount;

    @BeforeAll
    public static void setUp() {
        tr = new TransactionRepoDAO();
        ts = new TransactionServiceImpl(tr);
        ar = new AccountRepoDAO();
        as = new AccountServiceImpl(ar);
        ur = new UserRepoDAO();
        us = new UserServiceImpl(ur);

        testUser = us.addUser(new User("testUser123", "testPassword123"));
        testAccount = as.addAccount(new CheckingAccount("TestCheckingAccount", testUser.getUserID()));
    }

    @Test
    public void addTransactionTest() {
        Transaction testTransaction = ts.addTransaction(new Transaction("Withdrawal", 500, 300, testAccount.getAccountID()));

        assertNotNull(testTransaction);
        assertEquals("Withdrawal", testTransaction.getTransactionType());
        assertEquals(500, testTransaction.getBalanceBefore());
        assertEquals(300, testTransaction.getBalanceAfter());
    }
    @Test
    public void getAllTransactionsTest() {
        ts.addTransaction(new Transaction("Withdrawal", 500, 400, testAccount.getAccountID()));
        ts.addTransaction(new Transaction("Withdrawal", 400, 300, testAccount.getAccountID()));
        ts.addTransaction(new Transaction("Deposit", 300, 600, testAccount.getAccountID()));
        ts.addTransaction(new Transaction("Deposit", 600, 900, testAccount.getAccountID()));
        GenericLinkedList<Transaction> testTransactions = ts.getAllTransactions(testAccount.getAccountID());

        assertEquals(4, testTransactions.getSize());
    }

    @AfterAll
    public static void cleanUp() throws ResourceNotFoundException {
        us.deleteUser(testUser.getUserID());
    }
}
