import models.User;
import org.junit.jupiter.api.*;
import repositories.UserRepoDAO;
import services.UserServiceImpl;
import util.GenericLinkedList;
import util.ResourceNotFoundException;

import static org.junit.jupiter.api.Assertions.*;

public class UserServiceTests {
    private static UserRepoDAO ur;
    private static UserServiceImpl us;
    private static GenericLinkedList<User> testUsers = new GenericLinkedList<User>();

    @BeforeAll
    public static void setUp() {
        ur = new UserRepoDAO();
        us = new UserServiceImpl(ur);
    }

    @BeforeEach
    public void before() {}

    @Test
    public void addUserTest() {
         User testUser = ur.addUser(new User("TestUser1", "TestPassword1"));
         testUsers.add(testUser);

         assertEquals("TestUser1", testUser.getUsername());
         assertEquals("TestPassword1", testUser.getPassword());
         assertNotNull(testUser.getUserID());
    }
    @Test
    public void getUserByIDTest() {
        User testUser = ur.addUser(new User("TestUser2", "TestPassword2"));
        testUsers.add(testUser);
        User testUser2 = ur.getUserByID(testUser.getUserID());

        assertEquals("TestUser2", testUser2.getUsername());
        assertEquals("TestPassword2", testUser2.getPassword());
        assertEquals(testUser.getUserID(), testUser2.getUserID());
    }
    @Test
    public void getUserByUsernameTest() {
        User testUser = ur.addUser(new User("TestUser3", "TestPassword3"));
        testUsers.add(testUser);
        User testUser3 = ur.getUserByUsername("TestUser3");;

        assertEquals("TestUser3", testUser3.getUsername());
        assertEquals("TestPassword3", testUser3.getPassword());
        assertEquals(testUser.getUserID(), testUser3.getUserID());
    }
    @Test
    public void updatePasswordTest() {
        User testUser = ur.addUser(new User("TestUser4", "TestPassword4"));
        testUsers.add(testUser);
        User testUser4 = ur.updatePassword(testUser.getUserID(), "NewTestPassword4");

        assertEquals("NewTestPassword4", testUser4.getPassword());
        assertNotEquals("TestPassword4", testUser4.getPassword());
    }
    @Test
    public void deleteUserTest() throws ResourceNotFoundException {
        User testUser = ur.addUser(new User("TestUser5", "TestPassword5"));

        ur.deleteUser(testUser.getUserID());
        assertNull(ur.getUserByUsername("TestUser5"));
    }
    @Test
    public void verifyLoginTest() {
        User testUser = ur.addUser(new User("TestUser6", "TestPassword6"));
        testUsers.add(testUser);
        User testUser6 = ur.verifyLogin("TestUser6", "TestPassword6");

        assertNotNull(testUser6);
        assertEquals("TestUser6", testUser6.getUsername());
        assertEquals("TestPassword6", testUser6.getPassword());
    }
    @AfterAll
    public static void cleanUp() throws ResourceNotFoundException {
        for(int i=0; i<testUsers.getSize(); i++) {
            ur.deleteUser(testUsers.get(i).getUserID());
            System.out.println("Deleted: " + testUsers.get(i));
        }
        testUsers = new GenericLinkedList<User>();
    }
}
