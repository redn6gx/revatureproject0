package services;

import models.User;
import repositories.UserRepo;
import util.ResourceNotFoundException;

//business logic methods for service layer
public class UserServiceImpl implements UserService{
    private UserRepo ur;
    public UserServiceImpl(UserRepo ur) {
        this.ur = ur;
    }

    @Override
    public User addUser(User u) { return ur.addUser(u); }
    @Override
    public User getUserByID(int id) { return ur.getUserByID(id); }
    @Override
    public User getUserByUsername(String username) { return ur.getUserByUsername(username); }
    @Override
    public User updatePassword(int id, String newPassword) { return ur.updatePassword(id, newPassword); }
    @Override
    public User deleteUser(int id) throws ResourceNotFoundException { return ur.deleteUser(id); }
    @Override
    public User verifyLogin(String username, String password) { return ur.verifyLogin(username, password); }
}
