package services;

import models.Account;
import repositories.AccountRepo;
import util.GenericLinkedList;
import util.ResourceNotFoundException;

public class AccountServiceImpl implements AccountService{
    private AccountRepo ar;
    public AccountServiceImpl(AccountRepo ar) {
        this.ar = ar;
    }

    @Override
    public Account addAccount(Account acc) { return ar.addAccount(acc); }
    @Override
    public Account getAccount(int id) { return ar.getAccount(id); }
    @Override
    public GenericLinkedList<Account> getAllAccounts(int userID) { return ar.getAllAccounts(userID); }
    @Override
    public double updateBalance(double newBalance, int accountID) {
        Account acc = ar.updateBalance(newBalance, accountID);
//        if(acc != null) {
//            return acc.getBalance();
//        }
        return acc.getBalance();
    }
    @Override
    public Account deleteAccount(int id) throws ResourceNotFoundException { return ar.deleteAccount(id); }
}
