package services;

import models.Transaction;
import util.GenericLinkedList;

public interface TransactionService {
    public Transaction addTransaction(Transaction newTransaction);
    public GenericLinkedList<Transaction> getAllTransactions(int account_id);
}
