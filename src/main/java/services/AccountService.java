package services;

import models.Account;
import util.GenericLinkedList;
import util.ResourceNotFoundException;

public interface AccountService {
    public Account addAccount(Account acc);
    public Account getAccount(int id);
    public GenericLinkedList<Account> getAllAccounts(int userID);
    public double updateBalance(double newBalance, int accountID);
    public Account deleteAccount(int id) throws ResourceNotFoundException;
}
