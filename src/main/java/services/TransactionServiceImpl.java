package services;

import models.Transaction;
import repositories.TransactionRepo;
import util.GenericLinkedList;

public class TransactionServiceImpl implements TransactionService{
    private TransactionRepo tr;
    public TransactionServiceImpl(TransactionRepo tr) {
        this.tr = tr;
    }

    @Override
    public Transaction addTransaction(Transaction newTransaction) { return tr.addTransaction(newTransaction); }
    @Override
    public GenericLinkedList<Transaction> getAllTransactions(int account_id) { return tr.getAllTransactions(account_id); }
}
