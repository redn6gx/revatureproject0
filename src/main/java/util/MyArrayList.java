package util;

import java.lang.reflect.Array;

public class MyArrayList<T> {

    private T[] array;
    private int next = 0;
    private Class<T> myClass;

    public MyArrayList(Class<T> myClass) {
        array = createArray(myClass, 10);
        this.myClass = myClass;
    }

    public MyArrayList(Class<T> myClass, int length) {
        array = createArray(myClass, length);
    }

    private T[] createArray(Class<T> myClass, int length) {
        return (T[]) Array.newInstance(myClass, length);
    }


    public T get(int index) {
        return array[index];
    }


    public int add(T t) {

        if(next >= array.length) {
            resize();
        }

        array[next] = t;
        return next++;
    }

    public int add(T t, int index) {

        if(next <= 0 || index > next) {
            return add(t);
        }

        if(next >= array.length) {
            resize();
        }

        for(int i = (next - 1); i >= index; i--) {
            array[i + 1] = array[i];
        }
        array[index] = t;
        next++;

        return index;
    }

    public void resize() {
        T[] newArray = createArray(this.myClass, array.length * 2);
        System.arraycopy(array, 0, newArray, 0, array.length); //neat

        this.array = newArray;
    }

    public int size() {
        return next;
    }

    @Override
    public String toString() {
        String result = "[";

        for(int i = 0; i < size(); i++) {
            result += array[i];
            if(i < size() - 1) result += ", ";
        }
        result += "]";

        return result;
    }

    //isEmpty()
    public boolean isEmpty() {
        return size() == 0;
    }

}
