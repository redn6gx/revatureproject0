package util;

import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Properties;

//Here we create instance of DB and establish a connection
public class JDBC {
    private static Connection conn = null;

    public static Connection getConnection() {
        if(conn == null) {
            //Establish a new connection
            Properties props = new Properties();
            try {
                props.load(JDBC.class.getClassLoader().getResourceAsStream("connection.properties"));

                String endpoint = props.getProperty("endpoint");
                String url = "jdbc:postgresql://" + endpoint + "/postgres";
                String username = props.getProperty("username");
                String password = props.getProperty("password");

                conn = DriverManager.getConnection(url, username, password);
            } catch (IOException | SQLException e) {
                e.printStackTrace();
            }
        }
        return conn;
    }
}
