package repositories;

import models.Account;
import models.Transaction;
import util.GenericLinkedList;

public interface TransactionRepo {
    public Transaction addTransaction(Transaction newTransaction);
    public GenericLinkedList<Transaction> getAllTransactions(int account_id);
}
