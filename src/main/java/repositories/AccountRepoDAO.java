package repositories;

import models.Account;
import models.CheckingAccount;
import models.User;
import util.GenericLinkedList;
import util.JDBC;
import util.ResourceNotFoundException;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class AccountRepoDAO implements AccountRepo{
    Connection conn = JDBC.getConnection();

    @Override
    public Account addAccount(Account acc) {
        String sql = "INSERT INTO account VALUES (default,?,?,?) RETURNING *";

        try {
            //Set up PreparedStatement
            PreparedStatement ps = conn.prepareStatement(sql);

            //Set values for any Placeholders
            ps.setString(1, acc.getAccountName());
            ps.setDouble(2, acc.getBalance());
            ps.setInt(3, acc.getUserID());
            //Execute the query, store the results -> ResultSet
            ResultSet rs = ps.executeQuery();

            //Extract results our of ResultSet
            if(rs.next()) {
                return buildAccount(rs);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }
    @Override
    public Account getAccount(int id) {
        String sql = "SELECT * FROM account WHERE account_id=?";

        try {
            PreparedStatement ps = conn.prepareStatement(sql);
            ps.setInt(1, id);
            ResultSet rs = ps.executeQuery();

            if(rs.next()) {
                return buildAccount(rs);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }
    @Override
    public GenericLinkedList<Account> getAllAccounts(int userID) {
        GenericLinkedList<Account> accountList = new GenericLinkedList<>();
        String sql = "SELECT * FROM account WHERE user_id=?";

        try {
            PreparedStatement ps = conn.prepareStatement(sql);
            ps.setInt(1, userID);
            ResultSet rs = ps.executeQuery();

            while(rs.next()) {
                accountList.add(buildAccount(rs));
            }
            return accountList;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }
    @Override
    public Account updateBalance(double newBalance, int accountID) {
        String sql = "UPDATE account set balance=? WHERE account_id=? RETURNING *";

        try {
            PreparedStatement ps = conn.prepareStatement(sql);
            ps.setDouble(1, newBalance);
            ps.setInt(2, accountID);
            ResultSet rs = ps.executeQuery();

            if(rs.next()) {
                return buildAccount(rs);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }
    @Override
    public Account deleteAccount(int id) throws ResourceNotFoundException {
        String sql = "DELETE FROM account WHERE account_id=? RETURNING *";

        try {
            PreparedStatement ps = conn.prepareStatement(sql);
            ps.setInt(1, id);
            ResultSet rs = ps.executeQuery();

            if(rs.next()) {
                return buildAccount(rs);
            } else {
                throw new ResourceNotFoundException("Resource with id: " + id + " was not found in database.");
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    //Helper Method
    private Account buildAccount(ResultSet rs) throws SQLException {
        Account acc = new CheckingAccount();
        acc.setAccountID(rs.getInt("account_id"));
        acc.setAccountName(rs.getString("account_name"));
        acc.setBalance(rs.getDouble("balance"));
        acc.setUserID(rs.getInt("user_id"));

        return acc;
    }
}
