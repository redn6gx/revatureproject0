package repositories;

import models.User;
import util.ResourceNotFoundException;

//contains CRUD operation headers
public interface UserRepo {
    public User addUser(User u);
    public User getUserByID(int id);
    public User getUserByUsername(String username);
    public User updatePassword(int id, String newPassword);
    public User deleteUser(int id) throws ResourceNotFoundException;
    public User verifyLogin(String username, String password);
}
