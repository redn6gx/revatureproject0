package repositories;

import models.User;
import util.JDBC;
import util.ResourceNotFoundException;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

//implements CRUD operations
public class UserRepoDAO implements UserRepo{
    Connection conn = JDBC.getConnection();

    @Override
    public User addUser(User u) {
        String sql = "INSERT INTO users VALUES (default,?,?) RETURNING *";

        try {
            //Set up PreparedStatement
            PreparedStatement ps = conn.prepareStatement(sql);

            //Set values for any Placeholders
            ps.setString(1, u.getUsername());
            ps.setString(2, u.getPassword());

            //Execute the query, store the results -> ResultSet
            ResultSet rs = ps.executeQuery();

            //Extract results our of ResultSet
            if(rs.next()) {
                return buildUser(rs);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public User getUserByID(int id) {
        String sql = "SELECT * FROM users WHERE user_id = ?";

        try {
            PreparedStatement ps = conn.prepareStatement(sql);

            ps.setInt(1, id);
            ResultSet rs = ps.executeQuery();

            if(rs.next()) {
                return buildUser(rs);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return null;
    }

    @Override
    public User getUserByUsername(String username) {
        String sql = "SELECT * FROM users WHERE username = ?";

        try {
            PreparedStatement ps = conn.prepareStatement(sql);

            ps.setString(1, username);
            ResultSet rs = ps.executeQuery();

            if(rs.next()) {
                return buildUser(rs);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return null;
    }

    @Override
    public User updatePassword(int id, String newPassword) {
        String sql = "UPDATE users set password=? WHERE user_id=? RETURNING *";

        try {
            PreparedStatement ps = conn.prepareStatement(sql);

            ps.setString(1, newPassword);
            ps.setInt(2, id);
            ResultSet rs = ps.executeQuery();

            if(rs.next()) {
                return buildUser(rs);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return null;
    }

    @Override
    public User deleteUser(int id) throws ResourceNotFoundException {
        String sql = "DELETE FROM users WHERE user_id=? RETURNING *";

        try {
            PreparedStatement ps = conn.prepareStatement(sql);

            ps.setInt(1, id);
            ResultSet rs = ps.executeQuery();

            if(rs.next()) {
                return buildUser(rs);
            } else {
                throw new ResourceNotFoundException("Resource with id: " + id + " was not found in database.");
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public User verifyLogin(String username, String password) {
        String sql = "SELECT * FROM users WHERE username=? AND password=?";

        try {
            PreparedStatement ps = conn.prepareStatement(sql);

            ps.setString(1, username);
            ps.setString(2, password);
            ResultSet rs = ps.executeQuery();

            if(rs.next()) {
                return buildUser(rs);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    //Helper Method
    private User buildUser(ResultSet rs) throws SQLException {
        User u = new User();
        u.setUserID(rs.getInt("user_id"));
        u.setUsername(rs.getString("username"));
        u.setPassword(rs.getString("password"));
        return u;
    }

}
