package repositories;

import models.Account;
import util.GenericLinkedList;
import util.ResourceNotFoundException;

public interface AccountRepo {
    public Account addAccount(Account acc);
    public Account getAccount(int id);
    public GenericLinkedList<Account> getAllAccounts(int userID);
    public Account updateBalance(double newBalance, int accountID);
    public Account deleteAccount(int id) throws ResourceNotFoundException;
}
