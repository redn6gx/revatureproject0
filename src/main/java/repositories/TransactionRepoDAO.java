package repositories;

import models.Account;
import models.CheckingAccount;
import models.Transaction;
import util.GenericLinkedList;
import util.JDBC;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class TransactionRepoDAO implements TransactionRepo{
    Connection conn = JDBC.getConnection();

    @Override
    public Transaction addTransaction(Transaction newTransaction) {
        String sql = "INSERT INTO transactions VALUES (default,?,?,?,default,?) RETURNING *";

        try {
            PreparedStatement ps = conn.prepareStatement(sql);

            ps.setString(1, newTransaction.getTransactionType());
            ps.setDouble(2, newTransaction.getBalanceBefore());
            ps.setDouble(3, newTransaction.getBalanceAfter());
            ps.setInt(4, newTransaction.getAccountID());

            ResultSet rs = ps.executeQuery();

            if(rs.next()) {
                return buildTransaction(rs);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public GenericLinkedList<Transaction> getAllTransactions(int account_id){
        GenericLinkedList<Transaction> transactionList = new GenericLinkedList<>();
        String sql = "SELECT * FROM transactions WHERE account_id=?";

        try {
            PreparedStatement ps = conn.prepareStatement(sql);
            ps.setInt(1, account_id);
            ResultSet rs = ps.executeQuery();

            while(rs.next()) {
                transactionList.add(buildTransaction(rs));
            }
            return transactionList;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    //Helper Method
    private Transaction buildTransaction(ResultSet rs) throws SQLException {
//        Date date = Calendar.getInstance().getTime();
//        DateFormat dateFormat = new SimpleDateFormat("yyyy-mm-dd hh:mm:ss");
//        String strDate = dateFormat.format(date);

        Transaction resultTransaction = new Transaction();
        resultTransaction.setTransactionID(rs.getInt("transaction_id"));
        resultTransaction.setTransactionType(rs.getString("transaction_type"));
        resultTransaction.setBalanceBefore(rs.getDouble("balance_before"));
        resultTransaction.setBalanceAfter(rs.getDouble("balance_after"));
        resultTransaction.setTransactionDate(rs.getDate("transaction_date") + "");
        resultTransaction.setAccountID(rs.getInt("account_id"));

        return resultTransaction;
    }
}
