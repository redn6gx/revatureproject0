package models;

public class CheckingAccount extends Account{
    public CheckingAccount() {}

    public CheckingAccount(String accountName, int userID) {
        this.accountName = accountName;
        this.userID = userID;
        balance = 0;
    }
}
