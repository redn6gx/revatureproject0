package models;

import java.text.DecimalFormat;

public abstract class Account {
    protected int accountID;
    protected int userID;
    protected String accountName;
    protected double balance;

    //Constructors
    public Account(){
        balance = 0;
    }

    //Methods
    public boolean withdraw(double amount){
        if(amount > balance){
            System.out.println("TRANSACTION FAILED\nAmount: " + amount + ", Exceeds Maximum Funds: " + balance);
            return false;
        }else{
            balance -= amount;
            return true;
        }
    }
    public double deposit(double amount){
        balance += amount;
        return balance;
    }

    //Getters
    public double getBalance() {
        return balance;
    }
    public String getAccountName() {
        return accountName;
    }
    public int getUserID() { return userID; }
    public int getAccountID() { return accountID; }

    //Setters
    public void setAccountID(int accountID) { this.accountID = accountID; }
    public void setUserID(int userID) { this.userID = userID; }
    public void setAccountName(String accountName) { this.accountName = accountName; }
    public void setBalance(double balance) { this.balance = balance; }

    @Override
    public String toString() {
        DecimalFormat df = new DecimalFormat("0.00");
        //format balance to show 2 decimal places
        return accountName + " - Balance: $" + df.format(balance);
    }
}
