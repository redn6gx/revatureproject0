package models;

import java.text.DecimalFormat;

public class Transaction {
    private int transactionID;
    private String transactionType;
    private double balanceBefore;
    private double balanceAfter;
    private String transactionDate;
    private int accountID;

    public Transaction(){}

    public Transaction(String transactionType, double balanceBefore, double balanceAfter, int accountID) {
        this.transactionType = transactionType;
        this.balanceBefore = balanceBefore;
        this.balanceAfter = balanceAfter;
        this.accountID = accountID;
    }

    //getters and setters
    public int getTransactionID() {
        return transactionID;
    }
    public void setTransactionID(int transactionID) {
        this.transactionID = transactionID;
    }
    public String getTransactionType() {
        return transactionType;
    }
    public void setTransactionType(String transactionType) {
        this.transactionType = transactionType;
    }
    public double getBalanceBefore() {
        return balanceBefore;
    }
    public void setBalanceBefore(double balanceBefore) {
        this.balanceBefore = balanceBefore;
    }
    public double getBalanceAfter() {
        return balanceAfter;
    }
    public void setBalanceAfter(double balanceAfter) {
        this.balanceAfter = balanceAfter;
    }
    public String getTransactionDate() {
        return transactionDate;
    }
    public void setTransactionDate(String transactionDate) {
        this.transactionDate = transactionDate;
    }
    public int getAccountID() {
        return accountID;
    }
    public void setAccountID(int accountID) {
        this.accountID = accountID;
    }

    @Override
    public String toString() {
        DecimalFormat df = new DecimalFormat("0.00");
        double amount = transactionType.equals("Withdrawal") ? balanceBefore - balanceAfter : balanceAfter - balanceBefore;
        return "[ " +
                transactionType + " Amount: $" + df.format(amount) +
                " | $" + df.format(balanceBefore) +
                " ---> $" + df.format(balanceAfter) +
                " | Date: '" + transactionDate + " ]";
    }
}
