package app;

import models.Account;
import models.CheckingAccount;
import models.Transaction;
import models.User;
import repositories.*;
import services.*;
import util.GenericLinkedList;

import java.text.DecimalFormat;
import java.util.Scanner;

public class BankAppMethods {
    private static UserRepo ur = new UserRepoDAO();
    private static UserService us = new UserServiceImpl(ur);

    private static AccountRepo ar = new AccountRepoDAO();
    private static AccountService as = new AccountServiceImpl(ar);

    private static TransactionRepo tr = new TransactionRepoDAO();
    private static TransactionService ts = new TransactionServiceImpl(tr);

    private static Account currentAccount;

    public static void logIn(Scanner scan) {
        String username, password;
        User tempUser;

        System.out.println("======= LOG IN =======");
        while (true) {
            System.out.print("----------------------\nUsername: ");
            username = scan.nextLine();
            System.out.print("----------------------\nPassword: ");
            password = scan.nextLine();

            tempUser = us.verifyLogin(username, password);
            if (tempUser != null) {
                System.out.println("\nLogin Successful!\n");
                BankingApp.currentUser = tempUser;
                break;
            }
            System.out.println("Incorrect Username or Password");
        }

    }

    public static void signUp(Scanner scan) {
        String username, password1, password2;

        System.out.println("======= SIGN UP =======");
        while(true) {
            System.out.println("Enter a Username");
            username = scan.nextLine();

            //verify username does not exist
                //call get user by username method to check
            if(us.getUserByUsername(username) != null) {
                System.out.println("Username Already Taken!");
            }else { break; }
        }

        while(true) {
            System.out.println("Enter a Password");
            password1 = scan.nextLine();

            //maybe confirm password logic here
            System.out.println("Confirm Password");
            password2 = scan.nextLine();

            if(password1.equals(password2)) {
                break;
            }else {
                System.out.println("Passwords Did Not Match!");
            }
        }

        User u = us.addUser(new User(username, password1));
        System.out.println("\nAccount Successfully Created!\n");
    }

    public static GenericLinkedList<Account> accountList;
    public static void viewAccounts(Scanner scan) {
        accountList = as.getAllAccounts(BankingApp.currentUser.getUserID());
        int choice;
        while(true) {
            System.out.println("\nSelect an Account\n------------------\n0: Back");
            for (int i = 0; i < accountList.getSize(); i++) {
                System.out.println(i + 1 + ": " + accountList.get(i));
            }

            try {
                choice = Integer.parseInt(scan.nextLine()) - 1;
            } catch(NumberFormatException e) {
                System.out.println("Not a Valid Number Option!\n");
                continue;
            }

            if (choice >= 0 && choice < accountList.getSize()) {
                accountChoices(accountList.get(choice), scan);
            } else if(choice == -1){
                break;
            } else {
                System.out.println("Not a Valid Number Option!\n");
            }
        }
    }

    //Helper Method
    public static void accountChoices(Account acc, Scanner scan) {
        currentAccount = acc;
        int choice;
        DecimalFormat df = new DecimalFormat("0.00");

        while(true) {
            System.out.println("\n" + acc.getAccountName() + "\n------------------------\n" + "Balance: $" + df.format(currentAccount.getBalance()));
            System.out.println("0: Back\n1: Withdraw\n2: Deposit\n3: Transaction History");

            try {
                choice = Integer.parseInt(scan.nextLine());
            } catch(NumberFormatException e) {
                System.out.println("Not a Valid Number Option!\n");
                continue;
            }

            switch (choice) {
                case 0:
                    break;
                case 1:
                    withdraw(scan);
                    break;
                case 2:
                    deposit(scan);
                    break;
                case 3:
                    System.out.println("Transaction History\n--------------------");
                    history(scan);
                    break;
            }
            if (choice == 0) {
                break;
            }
        }
    }

    public static void history(Scanner scan) {
        GenericLinkedList transactions = ts.getAllTransactions(currentAccount.getAccountID());
        for(int i=0; i< transactions.getSize(); i++) {
            System.out.println(transactions.get(i));
        }
        System.out.println("Hit Enter to Continue...");
        String input = scan.nextLine();
        System.out.println("----------------------");
    }

    public static void withdraw(Scanner scan) {
        double currBalance = currentAccount.getBalance();
        while(true) {
            System.out.println("\nEnter the amount you would like to withdraw");
            double amount = Double.parseDouble(scan.nextLine());
            if (amount > currBalance) {
                System.out.println("\nAmount Exceeds Account Balance!");
            } else {
                as.updateBalance(currBalance - amount, currentAccount.getAccountID());
                accountList = as.getAllAccounts(BankingApp.currentUser.getUserID());
                currentAccount = as.getAccount(currentAccount.getAccountID());
                ts.addTransaction(new Transaction("Withdrawal", currBalance, currBalance-amount, currentAccount.getAccountID()));
                break;
            }
        }
    }

    public static void deposit(Scanner scan) {
        double currBalance = currentAccount.getBalance();
        while(true) {
            System.out.println("\nEnter the amount you would like to deposit");
            double amount = Double.parseDouble(scan.nextLine());
            as.updateBalance(currBalance + amount, currentAccount.getAccountID());
            accountList = as.getAllAccounts(BankingApp.currentUser.getUserID());
            currentAccount = as.getAccount(currentAccount.getAccountID());
            ts.addTransaction(new Transaction("Deposit", currBalance, currBalance+amount, currentAccount.getAccountID()));
            break;
        }
    }

    public static void openNewAccountOption(Scanner scan) {
        System.out.println("Enter a Name for This Account");
        String accountName = scan.nextLine();

        Account acc = new CheckingAccount(accountName, BankingApp.currentUser.getUserID());
        acc = as.addAccount(acc);

        System.out.println("\nNew account Successfully Created");
        System.out.println(acc + "\n");
    }
}
