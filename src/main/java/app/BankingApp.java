package app;

import models.Account;
import models.CheckingAccount;
import models.User;

import java.util.Scanner;

import static app.BankAppMethods.*;

public class BankingApp {
    //stores info for user currently logged in
    public static User currentUser = new User();

    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        int choice;

        //Main application loop
        while(true) {
            //Log in or Sign up
            System.out.println("======= Welcome! =======");
            System.out.println("Please Select an Option\n      1: Log in\n      2: Sign Up\n      3: Quit");

            try {
                choice = Integer.parseInt(scan.nextLine());
            } catch(NumberFormatException e) {
                System.out.println("Not a Valid Number Option!\n");
                continue;
            }

            switch(choice) {
                case 1:
                    logIn(scan);
                    break;
                case 2:
                    signUp(scan);
                    logIn(scan);
                    break;
                case 3:
                    scan.close();
                    System.out.println("Goodbye!");
                    return; //exit program
                default:
                    System.out.println("Not a Valid Number Option!\n");
                    continue;
            }

            while(true){
                //Home Menu Options
                System.out.println("======= HOME =======");
                System.out.println("Welcome " + currentUser.getUsername() + "!");
                System.out.println("1: Accounts\n2: Open New Account\n3: Log Out");

                try {
                    choice = Integer.parseInt(scan.nextLine());
                } catch(NumberFormatException e) {
                    System.out.println("Not a Valid Number Option!\n");
                    continue;
                }
                switch(choice) {
                    case 1:
                        viewAccounts(scan);
                        break;
                    case 2:
                        openNewAccountOption(scan);
                        break;
                    case 3:
                        currentUser = null;
                        break;
                    default:
                        System.out.println("Not a Valid Number Option!\n");
                        continue;
                }

                if(choice == 3) { break; }
            }//home screen loop



        }//main app loop
    }//main

}//Demo
