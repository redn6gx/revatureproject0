drop table if exists users
drop table if exists account
drop table if exists transactions

-- Create Tables
create table users (
	user_id serial not null,
	username varchar(30),
	password varchar(30),
	PRIMARY KEY (user_id)
);
create table account (
	account_id serial not null,
	account_name varchar(50),
	balance decimal(10,2),
	user_id serial not null,
	primary key (account_id),
	FOREIGN KEY(user_id)
	  	REFERENCES users(user_id)
	  		on delete cascade
);
create table transactions (
	transaction_id serial not null,
	transaction_type varchar(50),
	balance_before decimal(10,2),
	balance_after decimal(10,2),
	transaction_date date not null default current_date,
	account_id serial not null,
	primary key (transaction_id),
	foreign key(account_id)
		references account(account_id)
			on delete cascade
);

-- Populate with test values
insert into users values (default, 'bobby', 'test');
insert into users values (default, 'test', 'test');
insert into account values(default, 'MyAccount', 500, 1)
insert into transactions values(default, 'Withdrawal', 500, 400, default, 1);

-- Test Queries
select * from users
select username from users
select * from users where username = 'bobby'
select * from users where user_id = 1